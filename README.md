### lib_html2pdf
============

Html2Pdf (as a library for Content Management Systems [LEPTON CMS][1]) is a HTML to PDF converter written in PHP, and compatible with PHP 7.3 to 8.x.<br />
It allows the conversion of valid HTML in PDF format, to generate documents like invoices, documentation.

#### Requirements

* [LEPTON CMS][1]
* Server needs [PECL ssh2 >= 0.9.0]

#### Installation

* download latest [addon.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### First Steps

Go to https://html2pdf.fr/en/download and see what to do

#### LEPTON_CMS quick test example given
See [Html2Pdf at GitHub][4] for details!  

##### Since up 5.2.8.1 we can use the instance direct.  
Open a new (blank) Code2 section and paste:  
```php
$oPDF = lib_html2pdf::getInstance(); // optional "P", "A3" or "A6" and LANGUAGE

// 1. some settings e.g.
$oPDF->setAuthor("Aladin");
$oPDF->setCreator("LEPTON-CMS 7.2");
$oPDF->setTitle("Beispiel aus code2 Modul");
$oPDF->setSubject("Quittungsbeleg m.yyyy");
$oPDF->setKeywords("LEPTON-CMS, Aldus Beispiel, Quittungsbeleg");

// 2.1 a given string
// $oPDF->writeHTML('<h1>HelloWorld LEPTON_CMS</h1>This is my first test');

// 2.2 or a file e.g.
$oPDF->writeHTML(file_get_contents(LEPTON_PATH."/modules/lib_html2pdf/example/simple_pages.txt"));

// 3.0
// See details for the parameters at:
// https://github.com/spipu/html2pdf/blob/master/doc/output.md
$oPDF->output(LEPTON_PATH.MEDIA_DIRECTORY.'/lib_html2pdf_testfile.pdf', 'F');

echo "file written!";
```
some other methods e.g.
```php
// default font
$oPDF->setDefaultFont('Courier'); // Helvetica, Times, Courier, Zapf Dingbats 

// debug mode
$oPDF->setModeDebug();

// Bookmarks
// see: https://github.com/spipu/html2pdf/blob/master/doc/bookmark.md
$oPDF->createIndex('Summary', 25, 12, true, true);

// Get a list of all known page formats. (over 300!)
// ISO 216 A Series + 2 SIS 014711 extensions
$oPDF->getPageFormats();

// Get a list of all (core-)fonts.
$oPDF->getCoreFonts();

```
#### Instance and params
Since L* VII.2 the static "getInstance"-method holds the following params.  

 pos | meaning    | value                                                  | default
-----|------------|--------------------------------------------------------|---------------
   1 | orientation| Posible values are 'P' (portrait) or 'L' (landscape)   | 'P'
   2 | format     | E.g. "A4" (A0 ... A12) [more pageFormats][6]           | 'A4'
   3 | language   | E.g. "fr", default is the L* language                  | 'en'*
   4 | unicode    | 'true' or 'false'                                      | *true*
   5 | encoding   | 'UTF-8'                                                | 'UTF-8'
   6 | margins    | Array with the margins (left, top, right, bottom) [mm] | [5, 5, 5, 8]
   7 | pdfA       | 'true' or 'false'                                      | *false*
   
Prior versions of L* 7.2.0 the params have no effect!  

Typical forms are e.g.  
```php
$oPDF = lib_html2pdf::getInstance();
$oPDF = lib_html2pdf::getInstance('L');
$oPDF = lib_html2pdf::getInstance('L', 'A3');
$oPDF = lib_html2pdf::getInstance('L', 'A4', null, null, null, true); // for pdfA

```
 
[1]: https://lepton-cms.org "LEPTON CMS"
[2]: https://lepton-cms.com/lepador/libraries/lib_html2pdf.php
[3]: https://html2pdf.fr/en/download "html2pdf"
[4]: https://github.com/spipu/html2pdf/ "Html2Pdf at GitHub"
[5]: https://pecl.php.net/package/ssh2
[6]: https://gitlab.com/labby/lib_html2pdf/-/blob/master/lib/tecnickcom/tcpdf/include/tcpdf_static.php?ref_type=heads "pageFormats"
