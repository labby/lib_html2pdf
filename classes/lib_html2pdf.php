<?php

/**
 *  @module         Library html2pdf
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2025 cms-lab
 *  @license        Open Software License v. 3.0 (OSL-3.0)
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// This hack do the trick.
lib_html2pdf_loader::getInstance();

class lib_html2pdf extends Spipu\Html2Pdf\Html2Pdf
{
    use LEPTON_singleton;
    
    /**
     *  The own singelton instance.
     *  @type   instance
     */
    public static $instance;
    
    /**
     *  Call the parent constructor to initilize the object and all sub-instances.
     */
    public function __construct()
    {
        // [1]
        $aAllArguments = func_get_args();

        $sOrientation = $aAllArguments[0][0] ?? "P";
        $sFormat      = strtoupper($aAllArguments[0][1] ?? "A4");
        $sLanguage    = strtolower($aAllArguments[0][2] ?? LANGUAGE);
        
        // [1.1]
        $bUnicode     = $aAllArguments[0][3] ?? true;
        $sEncoding    = $aAllArguments[0][4] ?? 'UTF-8';
        $aMargins     = $aAllArguments[0][5] ?? [5, 5, 5, 8];
        $bPdfa        = $aAllArguments[0][6] ?? false;
        
        // [2]
        parent::__construct(
            $sOrientation,
            $sFormat,
            $sLanguage,
            $bUnicode,
            $sEncoding,
            $aMargins,
            $bPdfa
        );

        $this->pdf->SetDisplayMode('fullpage', 'TwoPageRight');

        // [3]
        LEPTON_handle::include_files('/modules/droplets/droplets.php');
    }
    
    public function writeHTML($html): void
    {
        // Some replacements
        $content = str_replace(
            ["[[ LEPTON_PATH ]]", "[[ LEPTON_URL ]]", "[[ MEDIA_DIRECTORY ]]"],
            [ LEPTON_PATH, LEPTON_URL, MEDIA_DIRECTORY ],
            $html
        );

        // Render droplets!
        $this->parseDroplets($content);
        
        try
        {
            // Call parent to process
            parent::writeHTML($content);
        } catch (Html2PdfException $e) {
            $html2pdf->clean();
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }
    
    /**
     * @param string    $sAuthor  Any valid string.
     */
    public function setAuthor(string $sAuthor): void
    {
        $this->pdf->setAuthor($sAuthor);
    }
    
    /**
     * @param string    $sCreator  Any valid string.
     */
    public function setCreator(string $sCreator): void
    {
        $this->pdf->setCreator($sCreator);
    }
    
    /**
     * @param string    $sTitle  Any valid string.
     */
    public function setTitle(string $sTitle): void
    {
        $this->pdf->setTitle($sTitle);
    }
    
    /**
     * @param string    $sSubject  Any valid string.
     */
    public function setSubject(string $sSubject): void
    {
        $this->pdf->setSubject($sSubject);
    }
    
    /**
     * @param string    $sKeywords  Any valid string, or list (comma separated).
     */
    public function setKeywords(string $sKeywords): void
    {
        $this->pdf->setKeywords($sKeywords);
    }

    public function getPageFormats(): array
    {
        return array_keys(tcpdf_static::$page_formats);
    }
    
    /**
     * Handle droplets.
     *
     * @param string $source The source to parse, call by reference.
     */
    protected function parseDroplets(string &$source): void
    {
        $pattern = "/\[\[(.*)\]\]/iU";
        $matches = [];

        $result = preg_match_all($pattern, $source, $matches, PREG_SET_ORDER);

        $aIgnore = [
            "page_cu",
            "page_nb",
            "date_d",
            "date_m",
            "date_y",
            "date_h",
            "date_i",
            "date_s",
            "&nbsp;LEPTON_PATH&nbsp;",
            "&nbsp;LEPTON_URL&nbsp;",
            "&nbsp;MEDIA_DIRECTORY&nbsp;"
        ];
        
        foreach ($matches as $index => $item)
        {
            if (in_array($item[1], $aIgnore))
            {
                unset($matches[$index]);
            }
        }

        foreach ($matches as $index => $item)
        {
            // $item[0] holds the complete droplet call
            $sTempDropletStr = $item[0];
            evalDroplets($sTempDropletStr);
            
            $source = str_replace($item[0], $sTempDropletStr, $source);
        }

    }
}