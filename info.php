<?php

/**
 *  @module         Library html2pdf
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2010-2025 cms-lab
 *  @license        Open Software License v. 3.0 (OSL-3.0)
 *  @license terms  see info.php of this addon
 *  @platform       see info.php of this addon
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$module_directory       = 'lib_html2pdf';
$module_name            = 'html2pdf Library';
$module_function        = 'library';
$module_version         = '5.2.8.3';
$module_platform        = '7.x';
$module_delete          =  true;
$module_author          = 'cms-lab';
$module_license         = 'https://github.com/spipu/html2pdf/blob/master/LICENSE.md';
$module_license_terms   = '-';
$module_description     = 'html2pdf allows the conversion of valid HTML in PDF format, to generate documents like invoices, documentation.';
$module_guid            = '66519ed9-d6c1-4339-bcee-7bf955cd2488';
$module_home            = 'https://cms-lab.com';

// get original lib code:
// 1. https://github.com/spipu/html2pdf	-> version 5.2.8
// 2. https://github.com/tecnickcom/TCPDF -> version 6.7.5
